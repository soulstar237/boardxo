/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;



/**
 *
 * @author informatics
 */
public class BoardArrayUI extends javax.swing.JFrame implements ActionListener{

    /**
     * Creates new form BoardArrayUI
     */
    public BoardArrayUI() {
        initComponents();
        initTableButtons();
        tableButton1.addActionListener(this);
        tableButton2.addActionListener(this);
        tableButton3.addActionListener(this);
        tableButton4.addActionListener(this);
        tableButton5.addActionListener(this);
        tableButton6.addActionListener(this);
        tableButton7.addActionListener(this);
        tableButton8.addActionListener(this);
        tableButton9.addActionListener(this);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        tableButton3 = new javax.swing.JButton();
        tableButton2 = new javax.swing.JButton();
        tableButton1 = new javax.swing.JButton();
        tableButton4 = new javax.swing.JButton();
        tableButton5 = new javax.swing.JButton();
        tableButton6 = new javax.swing.JButton();
        tableButton7 = new javax.swing.JButton();
        tableButton8 = new javax.swing.JButton();
        tableButton9 = new javax.swing.JButton();
        outputText = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(390, 325));
        setPreferredSize(new java.awt.Dimension(390, 350));
        getContentPane().setLayout(null);

        tableButton3.setText("-");
        tableButton3.setActionCommand("3");
        getContentPane().add(tableButton3);
        tableButton3.setBounds(260, 10, 60, 60);

        tableButton2.setText("-");
        tableButton2.setActionCommand("2");
        tableButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tableButton2ActionPerformed(evt);
            }
        });
        getContentPane().add(tableButton2);
        tableButton2.setBounds(140, 10, 60, 60);

        tableButton1.setText("-");
        tableButton1.setActionCommand("1");
        tableButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tableButton1ActionPerformed(evt);
            }
        });
        getContentPane().add(tableButton1);
        tableButton1.setBounds(20, 10, 60, 60);

        tableButton4.setText("-");
        tableButton4.setActionCommand("4");
        getContentPane().add(tableButton4);
        tableButton4.setBounds(20, 90, 60, 60);

        tableButton5.setText("-");
        tableButton5.setActionCommand("5");
        tableButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tableButton5ActionPerformed(evt);
            }
        });
        getContentPane().add(tableButton5);
        tableButton5.setBounds(140, 90, 60, 60);

        tableButton6.setText("-");
        tableButton6.setActionCommand("6");
        getContentPane().add(tableButton6);
        tableButton6.setBounds(260, 90, 60, 60);

        tableButton7.setText("-");
        tableButton7.setActionCommand("7");
        getContentPane().add(tableButton7);
        tableButton7.setBounds(20, 180, 60, 60);

        tableButton8.setText("-");
        tableButton8.setActionCommand("8");
        tableButton8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tableButton8ActionPerformed(evt);
            }
        });
        getContentPane().add(tableButton8);
        tableButton8.setBounds(140, 180, 60, 60);

        tableButton9.setText("-");
        tableButton9.setActionCommand("9");
        getContentPane().add(tableButton9);
        tableButton9.setBounds(260, 180, 60, 60);

        outputText.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        outputText.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        outputText.setText("Result");
        getContentPane().add(outputText);
        outputText.setBounds(40, 260, 260, 40);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void tableButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tableButton2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_tableButton2ActionPerformed

    private void tableButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tableButton5ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_tableButton5ActionPerformed

    private void tableButton8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tableButton8ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_tableButton8ActionPerformed

    private void tableButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tableButton1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_tableButton1ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(BoardArrayUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(BoardArrayUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(BoardArrayUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(BoardArrayUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new BoardArrayUI().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel outputText;
    private javax.swing.JButton tableButton1;
    private javax.swing.JButton tableButton2;
    private javax.swing.JButton tableButton3;
    private javax.swing.JButton tableButton4;
    private javax.swing.JButton tableButton5;
    private javax.swing.JButton tableButton6;
    private javax.swing.JButton tableButton7;
    private javax.swing.JButton tableButton8;
    private javax.swing.JButton tableButton9;
    // End of variables declaration//GEN-END:variables
    private javax.swing.JButton[][] tableButtons;
    @Override
    public void actionPerformed(ActionEvent e){
        outputText.setText(e.getActionCommand());
    }
 
        private void initTableButtons(){
            javax.swing.JButton[][] tableButtons ={
                {tableButton1,tableButton2,tableButton3},
                {tableButton4,tableButton5,tableButton6},
                {tableButton7,tableButton8,tableButton9}
            };
            this.tableButtons = tableButtons;
    }
}

